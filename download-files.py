import requests
import time
import rdflib
import pprint
import wave
import sys
from optparse import OptionParser
import os
import re
import json

'''
'''

resume = False
start_from = None
parser = OptionParser()
parser.add_option("-r","--resume",help="Resume from last downloaded file.",dest="resume",action="store_true")
parser.add_option("-s","--start",help="Start from this index.",dest="start_from",type="string")
parser.add_option("-o","--overwrite",help="Overwrite existing files.",dest="overwrite",action="store_false")
(options, args) = parser.parse_args()
if (options.overwrite is None):
    options.overwrite = False
json_file = open('config.json')
config = None
if (json_file is not None):
    config = json.load(json_file)
    '''
    with json_file:
        for (k, v) in config.items():
            print( "{0} : {1}".format(k, v) )
    '''
else:
    print("could not open config.json file")      
    exit( 1 )

if options.resume is not None and options.start_from is not None:
    print("--start_from and --resume options are mutually exclusive.")
    sys.exit(1)
archive_dir = config[".".join(["destination", sys.platform])]
## according to the BBC site, this is the starting sample number
## start_sample = 7076051
start_sample = int(config["start"])
tracker_file = os.path.join( archive_dir, "last-download.txt")
maxPages = 11
default_baseUrl = "http://bbcsfx.acropolis.org.uk"
baseUrl = config["base_url"]
sleepTime = 0.1
## fake your UA to make it look like you're sitting in front of your computer downloading each file...
headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0' }
g = rdflib.Graph()

if options.start_from is None:
    start_from = start_sample
else:
    start_from = int(options.start_from)
    print("Starting from {0}".format(start_from))
## These are the original start and ending ids
##ids = (7076051, 7001001)

if options.resume:
    try:
        with open(tracker_file) as tf:
            start_from = int(tf.readline())
            print("Resuming from %08d" % start_from)
    except(IOError):
        print("could not open {0}".format(tracker_file))
## Subsequent runs may start at a different place.
ids = (start_from, 7001001)

categories = ["Train", "Bird_vocalization", 'Engine']
## page 1: 07076051
## page 200: 07045011
## page 300: 07037215
## page 400: 07025008
## page 500: 07015117
## page 600: 07003085
## page 620: 07001001
## train sample: 07071330
## last sample id: 00008000
## last sample id: 00008000
## 24 resaults per page
## Fun categories: Romania, Trains, Radio
## sample WAV link: ## http://bbcsfx.acropolis.org.uk/assets/07076050.wav

def write_wav_file(target_file, content):
    ww = wave.open(target_file,'wb')
    ww.setframerate(44100)
    ww.setnchannels(2)
    ww.setsampwidth(2)
    if (r.content is not None and ww is not None):
        ww.writeframes(content)
        ww.close()

currId = ids[0]

while currId >= ids[1]:
    url = "{0}/{1}.ttl".format(baseUrl, "%08d" % currId)
    r = requests.get(url)
    if r.ok:
        g = rdflib.Graph()
        result = g.parse(url)
        ##for stmt in g:
        ##    pprint.pprint(stmt)
        file_name = None
        subjects = set()
        label = None
        wave_url = None
        label = None
        for subj, pred, obj in g:
            ##print("Subj: {0}, Pred: {1}, Obj: {2}".format(subj, pred, obj))
            if (subj, pred, obj) not in g:
                raise Exception("It better be!")
            content_match = re.search('content', pred, re.IGNORECASE)
            if (content_match):
                print("Wav file URL is {0}".format(obj))
                wave_url = obj
            subject_match = re.search('subject', pred, re.IGNORECASE)
            for category in categories:
                category_match = re.match(".*/(\w+)$", obj, re.IGNORECASE)
                if (subject_match and category_match):
                    subjects.add(category_match.group(1))
        file_name = os.path.join(archive_dir, "{0}-{1}.wav".format("_".join(sorted(list(subjects))), currId))
        if (not options.overwrite) and os.path.isfile(file_name):
            print ("Skipping file {0}. File exists and overwrite is {1}.".format(file_name, options.overwrite))
            currId -= 1
            continue
        if len(subjects) > 0 and wave_url is not None:
            r = requests.get(wave_url)
            if r.content is not None:
                ##token_list = list(subjects)
                print("Saving file {0}".format(file_name))
                write_wav_file(file_name, r.content)
                fh = open(tracker_file, 'w')
                fh.write("%08d" % currId)
                fh.close()
    else:
        print("Unable to process URL {0}".format(url))
    time.sleep(sleepTime)
    currId -= 1
