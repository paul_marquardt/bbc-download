from pathlib import Path, PurePath
import subprocess
from subprocess import PIPE
import os
import re
from optparse import OptionParser

'''
    This python script removes the noise burst from sound files offered by the BBC Onoine Sound Effect
Library. This library contains the entire BBC sound archive. The main caveat is that each file contains
an audible noise burst in the first few hundredths of a second of each file. This noise burst is sonified
meta-data about the file, containing a text description and file duration. This script strips the noise,
saving the file with a new name.
'''
default_extension="*.wav"
soundfile_extension=default_extension
fade_start=0.15
fade_time=0.05

parser = OptionParser()
parser.add_option("-i","--inputdir",help="Read .wav files from this directory.",dest="input",type="string")
parser.add_option("-o","--outputdir",help="Write the stripped audio files to this directory.",dest="output",type="string")
parser.add_option("-e","--extension",help="Specify the file extension, starting with the '*.'.",dest="extension",type="string")
(options, args) = parser.parse_args()
if (options.input is None):
    print ("Refusing to run without input directory specified.")
    exit(1)
if (options.output is None):
    print ("Refusing to run without output directory specified.")
    exit(1)
if (options.extension is not None):
    soundfile_extension=options.extension
    print ("Using file extension {0}.".format(soundfile_extension))

command_params=["-af", "afade=in:st={0}:d={1}".format( fade_time, fade_start )]

def run():
    input_path = Path( options.input ).glob(soundfile_extension)
    print("{0} : {1} : {2}".format(str(input_path), options.input, soundfile_extension))
    for file in input_path:
        out_file = "{0}{1}{2}-stripped{3}".format( options.output, os.path.sep, file.stem, file.suffix )
        command_spec = ["ffmpeg", "-i", str(options.input / file)]
        command_spec.extend(command_params)
        command_spec.append(out_file)
        print(" ".join(command_spec))
        p = subprocess.run(command_spec, stdout=PIPE, stderr=PIPE)
        if ( p.returncode == 0 ):
            print("==================== Begin printing output ====================")
            for line in str(p.stderr).split(r'\n'):
                print( ">>> {0}".format(line) )
            print("==================== End printing output ====================")
if ( __name__ == "__main__"):
    run()